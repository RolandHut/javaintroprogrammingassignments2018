/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    
    
    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return null;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return null;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
        return 0;
    }
    
}
