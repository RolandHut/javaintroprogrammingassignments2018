/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section2_syntax.part2_operators;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class WeightUnitsSolver {
    /**
     * main to be used for testing purposes
     * @param args 
     */
    public static void main(String[] args) {
        WeightUnitsSolver wus = new WeightUnitsSolver();
        wus.convertFromGrams(1000);
    }
    
    /**
     * Returns the number of Pounds, Ounces and Grams represented by this quantity of grams,
     * encapsulated in a BritishWeightUnits object.
     * @param grams
     * @return a BritishWeightUnits instance
     * @throws IllegalArgumentException when the Grams quantity is 
     */
    public BritishWeightUnits convertFromGrams(int grams) {
        //change this variable to get a correct testing condition
        final int gramsTest = 0;
        if (grams <= gramsTest) {
            throw new IllegalArgumentException("Error: grams should be above 0. Given: grams=" + grams);
        }

        int leftoverGrams = grams % 454;
        int pounds = (grams - leftoverGrams) / 454;
        int finalGrams = leftoverGrams % 28;
        int ounces = (leftoverGrams - finalGrams) / 28;

        //solve the pounds, ounces and grams, create and return a BritishWeightUnits instance

        return new BritishWeightUnits(pounds, ounces, finalGrams);
    }
}
