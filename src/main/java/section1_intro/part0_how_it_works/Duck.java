package section1_intro.part0_how_it_works;

/**
 * Creation date: Jun 27, 2017
 *
 * @version 0.01
 * @autor Michiel Noback (&copy; 2017)
 */
public class Duck {
    public int swimSpeed;
    public String name;

    public Duck(int swimSpeed, String name) {
        this.swimSpeed = swimSpeed;
        this.name = name;
    }
    public int getSwimSpeed() {
        return swimSpeed;
    }

    public void setSwimSpeed(int swimSpeed) {
        this.swimSpeed = swimSpeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
